package exercise;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String getForwardedVariables(String content) {
        return Stream.of(content.split("\\n"))
                .filter(line -> line.startsWith("environment="))
                .flatMap(line -> Arrays.stream(line.split(",")))
                .filter(line -> line.contains("X_FORWARDED_"))
                .map(line -> line.replaceAll(".*?X_FORWARDED_|,.*|\"", ""))
                .collect(Collectors.joining(","));
    }
}
//END
