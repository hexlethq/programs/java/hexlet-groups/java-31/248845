package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int j = 1; j < arr.length; j++) {
                //swap values
                if (arr[j - 1] > arr[j]) {
                    arr[j] += arr[j - 1];
                    arr[j - 1] = arr[j] - arr[j - 1];
                    arr[j] -= arr[j - 1];
                    isSorted = false;
                }
            }
        }
        return arr;
    }

    public static int[] sortBySelection(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int indOfMin = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[indOfMin]) {
                    indOfMin = j;
                }
            }
            if (indOfMin > i)  {
                arr[indOfMin] += arr[i];
                arr[i] = arr[indOfMin] - arr[i];
                arr[indOfMin] = arr[indOfMin] - arr[i];
            }
        }
        return arr;
    }
    // END
}
