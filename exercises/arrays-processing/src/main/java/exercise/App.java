package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        int index = -1;
        int maxNeg = 0;

        for (int i = 0; i < arr.length; i++) {
            boolean isValid = arr[i] < 0 && ((maxNeg < arr[i]) || (index == -1));

            if (isValid) {
                maxNeg = arr[i];
                index = i;
            }
        }
        return index;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) return arr;

        int average = getAverage(arr);
        int[] result = new int[getLength(arr, average)];
        int index = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= average) {
                result[index] = arr[i];
                index++;
            }
        }
        return result;
    }
    private static int getAverage(int[] arr) {
        int result = 0;
        for (int num : arr) {
            result += num;
        }
        return result / arr.length;
    }
    private static int getLength(int[] arr, int average) {
        int length = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= average) length++;
        }
        return length;
    }
//    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int getSumBeforeMinAndMax(int[] arr) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int indexOfMin = -1;
        int indexOfMax= -1;
        int[] rangeOfNumbers = {};
        int result = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
                indexOfMin = i;
            }
            if (arr[i] > max) {
                max = arr[i];
                indexOfMax = i;
            }
        }

        if (indexOfMax == indexOfMin) {
            return 0;
        } else if (indexOfMax > indexOfMin) {
            rangeOfNumbers = Arrays.copyOfRange(arr, indexOfMin + 1, indexOfMax);
        } else {
            rangeOfNumbers = Arrays.copyOfRange(arr, indexOfMax + 1, indexOfMin);
        }

        for (int num : rangeOfNumbers) {
            result += num;
        }
        return result;
    }
    // END
}
