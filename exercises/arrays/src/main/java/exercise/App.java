package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        if (arr.length == 0) return arr;

        int[] result = new int[arr.length];
        int index = 0;

        for (int i = arr.length - 1; i >= 0; i--) {
            result[index] = arr[i];
            index++;
        }
        return result;
    }

    public static int mult(int[] arr) {
        int result = 1;
        for (int j : arr) {
            result *= j;
        }
        return result;
    }

    /*
    public static int[] flattenMatrix(int[][] arr) {
        int[] result = {};
        int index = 0;
        int newLength = 0;
        boolean lengthIsKnown = false;

        for (int i = 0; i < arr.length; i++) {
            if (!lengthIsKnown) {
                newLength += arr[i].length;
                if (i == arr.length - 1) {
                    result = new int[newLength];
                    lengthIsKnown = true;
                    i = -1;
                }
                continue;
            }
            for (int j = 0; j < arr[i].length; j++) {
                result[index] = arr[i][j];
                index++;
            }
        }
        return result;
    }
     */
    // END
}
