package exercise;

import java.util.Arrays;

// BEGIN
class Kennel {
    private static String[][] kennel = new String[0][0];

    public static void addPuppy(String[] puppy) {
        kennel = Arrays.copyOf(kennel, kennel.length + 1);
        kennel[kennel.length - 1] = puppy;
    }

    public static void addSomePuppies(String[][] puppies) {
        kennel = Arrays.copyOf(kennel, kennel.length + puppies.length);
        System.arraycopy(puppies, 0, kennel, kennel.length - puppies.length, puppies.length);
    }

    public static int getPuppyCount() {
        return kennel.length;
    }

    public static boolean isContainPuppy(String name) {
        for (String[] puppy : kennel) {
            if (name.equals(puppy[0])) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() { return kennel; }

    public static String[] getNamesByBreed(String breed) {
        String[] names = new String[kennel.length];
        int index = 0;

        for (String[] puppy : kennel) {
            if (puppy[1].equals(breed)) {
                names[index++] = puppy[0];
            }
        }
        return Arrays.copyOf(names, index);
    }

    public static void resetKennel() { kennel = new String[0][0]; }

    public static boolean removePuppy(String name) {
        boolean isContain = false;

        for (int i = 0; i < kennel.length; i++) {
            if (name.equals(kennel[i][0])) {
                isContain = true;
            }

            if (isContain) {
                if (kennel.length == i + 1) {
                    kennel = Arrays.copyOf(kennel, i);
                } else {
                    kennel[i] = kennel[i + 1];
                }
            }
        }
        return isContain;
    }
}
// END
