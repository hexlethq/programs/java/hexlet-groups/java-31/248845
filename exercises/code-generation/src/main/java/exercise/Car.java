package exercise;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

// BEGIN
@Data
@AllArgsConstructor
@Value
// END
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;

    // BEGIN
    public String serialize() {
        String json = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static Car unserialize(String json) {
        Car car = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            car = mapper.readValue(json, Car.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return car;
    }
    // END
}
