package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if ((a <= 0 || b <= 0 || c <= 0) ||
            (a + b <= c || a + c <= b || b + c <= a)) {
            return "Треугольник не существует";
        } else if ( a == b && b == c) {
            return "Равносторонний";
        } else if (a != b && a != c && b != c) {
            return "Разносторонний";
        } else {
            return "Равнобедренный";
        }
    }
    // END
}
