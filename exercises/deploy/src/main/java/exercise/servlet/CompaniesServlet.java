package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        List<String> companies = getCompanies();
        String companiesString = "";
        String searchValue = request.getParameter("search");
        if (searchValue != null && !searchValue.isEmpty()) {
            companies = companies.stream()
                    .filter(company -> company.contains(searchValue))
                    .collect(Collectors.toList());
        }
        companiesString = String.join("\n", companies);

        if (companiesString.isEmpty()) {
            companiesString = "Companies not found";
        }

        PrintWriter out = response.getWriter();
        out.println(companiesString);
        out.close();
        // END
    }
}
