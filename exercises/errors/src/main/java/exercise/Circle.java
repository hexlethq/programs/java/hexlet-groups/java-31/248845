package exercise;

import java.io.FileNotFoundException;

// BEGIN
class Circle {
    private final int radius;
    private final Point point;


    Circle(Point point, int radius) {
        this.point = point;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (radius >= 0) {
            return Math.PI * Math.pow(radius, 2);
        } else {
            throw new NegativeRadiusException("Radius of circle can't be negative. Please, try again.");
        }
    }
}
// END
