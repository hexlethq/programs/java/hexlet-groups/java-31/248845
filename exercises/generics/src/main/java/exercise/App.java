package exercise;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
        List<Map<String, String>> found = new ArrayList<>();
        for (Map<String, String> book : books) {
            if (isEqualByAttributes(book, where)) {
                found.add(book);
            }
        }
        return found;
    }

    private static boolean isEqualByAttributes(Map<String, String> book, Map<String, String> attributes) {
        for (String key : attributes.keySet()) {
            if (!book.containsKey(key)) {
                return false;
            }
            if (!(attributes.get(key).equals(book.get(key)))) {
                return false;
            }
        }
        return true;
    }
}
//END
