package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {
    private final String body;
    private final List<Tag> children;

    protected PairedTag(String name, Map<String, String> attributes,
                        String body, List<Tag> children) {
        super(name, attributes);
        this.body = body;
        this.children = children;
    }

    @Override
    public String toString() {
        StringBuilder children = new StringBuilder();
        for (Tag child: this.children) {
            children.append(child.toString());
        }
        return String.format("%s%s%s</%s>", super.toString(), children, this.body, super.getName());
    }
}
// END
