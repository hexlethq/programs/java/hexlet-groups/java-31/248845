package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag {
    protected SingleTag(String tagName, Map<String, String> attributes) {
        super(tagName, attributes);
    }
}
// END
