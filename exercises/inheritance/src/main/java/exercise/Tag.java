package exercise;

import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
public abstract class Tag {
    private final String name;
    private final Map<String, String> attributes;

    protected Tag(String tagName, Map<String, String> attributes) {
        this.name = tagName;
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        StringBuilder attributes = new StringBuilder();
        for (String key: this.attributes.keySet()) {
            attributes
                    .append(" ")
                    .append(key)
                    .append("=\"")
                    .append(this.attributes.get(key))
                    .append("\"");
        }
        return String.format("<%s%s>", this.name, attributes);
    }
}
// END
