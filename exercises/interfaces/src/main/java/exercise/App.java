package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> apartments, int count) {
        if (apartments.size() < count) {
            return new ArrayList<>();
        }
        return apartments.stream()
                .sorted(Home::compareTo)
                .map(Object::toString)
                .collect(Collectors.toList()).subList(0, count);
    }
}
// END
