package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class ReversedSequence implements CharSequence {
    public ReversedSequence(String charSequence) {
        StringBuilder sb = new StringBuilder();
        int index = 0;
        for (int i = charSequence.length() - 1; i >= 0 ; i--) {
            sb.append(charSequence.charAt(i));
        }
        reversedSequence = sb.toString();
    }

    private final String reversedSequence;

    @Override
    public int length() {
        return reversedSequence.length();
    }

    @Override
    public char charAt(int i) {
        return reversedSequence.charAt(i);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return reversedSequence.subSequence(i, i1);
    }

    @Override
    public String toString() {
        return reversedSequence;
    }
}
// END
