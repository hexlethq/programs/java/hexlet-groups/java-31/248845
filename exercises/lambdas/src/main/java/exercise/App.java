package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        Stream<String[]> enlargedHorizontal = Stream.of(image)
                .map(arr -> enlarge(Stream.of(arr))
                .toArray(String[]::new));
        String[][] enlargedVertical = enlarge(enlargedHorizontal)
                .toArray(String[][]::new);
        return enlargedVertical;
    }

    private static <T> Stream<T> enlarge(Stream<T> arr) {
        return arr.flatMap(x -> Stream.of(x, x));
    }
}
// END
