package exercise;

import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.time.LocalDate;
import java.util.stream.Collectors;

// BEGIN
class Sorter {

    public static List<Map<String, String>> sorter(List<Map<String, String>> users) {
        return users.stream()
                .filter(user -> user.get("gender").equals("male"))
                .sorted(new UserComparator())
                .collect(Collectors.toList());
    }
}

class UserComparator implements Comparator<Map<String, String>> {
    @Override
    public int compare(Map<String, String> users, Map<String, String> t1) {
        return LocalDate.parse(users.get("birthday")).compareTo(LocalDate.parse(t1.get("birthday")));
    }
}
// END
