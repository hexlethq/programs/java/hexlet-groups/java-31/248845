package exercise;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

// BEGIN
class SorterTest {
    @Test
    void testSorter1() {

        List<Map<String, String>> people = List.of(
                Map.of("name", "Ivan Pavlov", "birthday", "2001-10-11", "gender", "male"),
                Map.of("name", "Dmitriy Savilov", "birthday", "1988-04-27", "gender", "male"),
                Map.of("name", "Anna Kovalchuk", "birthday", "1993-02-17", "gender", "female"),
                Map.of("name", "Azbek Bahiev", "birthday", "1971-11-09", "gender", "male"),
                Map.of("name", "Ekaterina Solovyeva", "birthday", "1997-08-11", "gender", "female"),
                Map.of("name", "Oleg Taktarov", "birthday", "1967-08-26", "gender", "male")
        );

        List<Map<String, String>> expected = List.of(
                Map.of("name", "Oleg Taktarov", "birthday", "1967-08-26", "gender", "male"),
                Map.of("name", "Azbek Bahiev", "birthday", "1971-11-09", "gender", "male"),
                Map.of("name", "Dmitriy Savilov", "birthday", "1988-04-27", "gender", "male"),
                Map.of("name", "Ivan Pavlov", "birthday", "2001-10-11", "gender", "male")
        );

        List<Map<String, String>> actual = Sorter.sorter(people);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testSorter2() {

        List<Map<String, String>> people = List.of(
                Map.of("name", "Olga Lobova", "birthday", "2002-01-30", "gender", "female"),
                Map.of("name", "Igor Bedenko", "birthday", "1995-04-04", "gender", "male"),
                Map.of("name", "Inna Vakulova", "birthday", "1959-06-21", "gender", "female"),
                Map.of("name", "Andrey Shumov", "birthday", "1982-04-26", "gender", "male"),
                Map.of("name", "Elisabeth Hamilton", "birthday", "1870-10-05", "gender", "female"),
                Map.of("name", "Maxim Halmutov", "birthday", "1980-02-02", "gender", "male"),
                Map.of("name", "Nikolay Ironfist", "birthday", "1997-01-01", "gender", "male"),
                Map.of("name", "Ivan Ivanov", "birthday", "2001-11-11", "gender", "male")
        );

        List<Map<String, String>> expected = List.of(
                Map.of("name", "Maxim Halmutov", "birthday", "1980-02-02", "gender", "male"),
                Map.of("name", "Andrey Shumov", "birthday", "1982-04-26", "gender", "male"),
                Map.of("name", "Igor Bedenko", "birthday", "1995-04-04", "gender", "male"),
                Map.of("name", "Nikolay Ironfist", "birthday", "1997-01-01", "gender", "male"),
                Map.of("name", "Ivan Ivanov", "birthday", "2001-11-11", "gender", "male")
        );

        List<Map<String, String>> actual = Sorter.sorter(people);

        assertThat(actual).isEqualTo(expected);
    }
}
// END


