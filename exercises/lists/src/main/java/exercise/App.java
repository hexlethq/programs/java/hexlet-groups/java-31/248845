package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// BEGIN
public class App {
    public static boolean scrabble(String charset, String checkWord) {
        if (charset.length() < checkWord.length()) {
            return false;
        }
        List<String> chars = new ArrayList<>(Arrays.asList(charset.split("")));
        String[] word = checkWord.toLowerCase().split("");
        for (String letter : word) {
            if (!(chars.remove(letter))) {
                return false;
            }
        }
        return true;
    }
}
//END
