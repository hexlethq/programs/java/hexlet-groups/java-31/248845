package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = ((number % 2 != 0) && (number >= 1001));
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        System.out.println((number % 2) == 0 ? "yes" : "no");
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        String quarter = "Number should be in 0-59";
        if (minutes >= 0 && minutes <= 14) quarter = "First";
        else if (minutes >= 15 && minutes <= 30) quarter = "Second";
        else if (minutes >= 31 && minutes <= 45) quarter = "Third";
        else quarter = "Fourth";
        System.out.println(quarter);
        // END
    }
}
