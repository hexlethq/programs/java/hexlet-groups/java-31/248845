package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String result = "";
        if (Character.isAlphabetic(str.charAt(0))) {
            result += str.charAt(0);
        }
        for (int i = 1; i < str.length(); i++) {
            if ((str.charAt(i - 1) == ' ') &&
                Character.isAlphabetic(str.charAt(i))) {
                    result += str.charAt(i);
            }
        }
        return result.toUpperCase();
    }
    // END
}