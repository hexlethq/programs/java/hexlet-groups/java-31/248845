package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// BEGIN
class App {
    public static Map<String, Integer> getWordCount(String sentence) {
        Map<String, Integer> wordFrequency = new HashMap<>();
        if (sentence.length() == 0) {
            return wordFrequency;
        }
        String[] words = sentence.split(" ");
        for (String word : words) {
            int frequency = 1;
            frequency += wordFrequency.getOrDefault(word, 0);
            wordFrequency.put(word, frequency);
        }
        return wordFrequency;
    }

    public static String toString(Map<String, Integer> wordFrequency) {
        if (wordFrequency.isEmpty()) {
            return "{}";
        }
        StringBuilder mapAsString = new StringBuilder("{\n");
        Set<Map.Entry<String, Integer>> entries = wordFrequency.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            mapAsString.append(String.format("  %s: %d%n",
                    entry.getKey(),
                    entry.getValue()));
        }
        mapAsString.append("}");
        return mapAsString.toString();
    }
}
//END
