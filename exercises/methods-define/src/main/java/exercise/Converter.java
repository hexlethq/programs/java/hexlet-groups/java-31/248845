package exercise;

class Converter {
    // BEGIN
    public static int convert(int value, String unit) {
        if (unit.equals("b")) return Math.round(value * 1024);
        else if (unit.equals("Kb")) return Math.round(value / 1024);
        else return 0;
    }

    public static void main(String[] args) {
        System.out.println(String.format("%1$s %2$s %3$s","10 Kb =",  Converter.convert(10, "b"), "b"));
    }
    // END
}