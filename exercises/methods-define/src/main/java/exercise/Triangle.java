package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double firstSide, double secondSide, double angle) {
        double angleAsRadian = angle * Math.PI / 180;
        return firstSide * secondSide / 2 * Math.sin(angleAsRadian);
    }

    public static void main(String[] args) {
        System.out.println(Triangle.getSquare(4, 5, 45));
    }
    // END
}
