package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String stars = "*".repeat(starsCount);
        String lastDigits = cardNumber.substring(12);
        System.out.println(stars + lastDigits);
        // END
    }
}
