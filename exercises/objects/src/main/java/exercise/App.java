package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] arr) {
        if (arr.length == 0) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder("<ul>\n");
            for (String s : arr) {
                sb.append(String.format("  <li>%s</li>\n", s));
            }
            return sb.append("</ul>").toString();
        }
    }

    public static String getUsersByYear(String[][] arr, int year) {
        String[] list = new String[arr.length];
        int counter = 0;

        for (String[] person : arr) {
            LocalDate date = LocalDate.parse(person[1]);
            if (date.getYear() == year) {
                list[counter++] = person[0];
            }
        }
        return buildList(Arrays.copyOf(list, counter));
    }

    public static String getYoungestUser(String[][] users, String date) {
        // BEGIN
        //format the date
        LocalDate dateArgument = LocalDate.parse(date,
                DateTimeFormatter.ofPattern("dd LLL yyyy"));

        int indOfYoungest = -1;
        boolean isFirstYoungest = true;

        for (int i = 0; i < users.length; i++) {
            LocalDate dateOfUser = LocalDate.parse(users[i][1]);
            if (dateOfUser.compareTo(dateArgument) < 0) {
                if (isFirstYoungest) {
                    indOfYoungest = i;
                    isFirstYoungest = false;
                } else if (dateOfUser.compareTo(LocalDate.parse(users[indOfYoungest][1])) > 0) {
                    indOfYoungest = i;
                }
            }
        }

        if (indOfYoungest == -1) {
            return "";
        } else {
            return users[indOfYoungest][0];
        }
        // END
    }
// END
}
