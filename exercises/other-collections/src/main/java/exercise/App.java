package exercise;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        Map<String, String> result = new LinkedHashMap<>();
        Set<String> keysOfData2 = data2.keySet();
        for (String key : data1.keySet()) {
            String state = !(data2.containsKey(key)) ?
                    "deleted" : data2.get(key).equals(data1.get(key)) ?
                    "unchanged" : "changed";
            result.put(key, state);
            keysOfData2.remove(key);
        }
        for (String key : keysOfData2) {
            result.put(key, "added");
        }

        return Stream.of(result.keySet())
                .flatMap(Collection::stream)
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toMap(Function.identity(),
                        result::get,
                        (a, b) -> a,
                        LinkedHashMap::new));
    }
}
//END
