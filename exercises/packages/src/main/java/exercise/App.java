// BEGIN
package exercise;

import exercise.geometry.*;

class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        return new double[] {
                (segment[0][0] + segment[1][0]) / 2,
                (segment[0][1] + segment[1][1]) / 2,
        };
    }

    public static double[][] reverse(double[][] segment) {
        return new double[][] {
                { segment[1][0], segment[1][1] },
                { segment[0][0], segment[0][1] },
        };
    }

    public static boolean isBelongToOneQuadrant(double[][] segment) {
        if (segment[0][0] * segment[1][0] <= 0) {
            return false;
        } else if (segment[0][1] * segment[1][1] <= 0) {
            return false;
        } else { return true; }
    }
}
// END
