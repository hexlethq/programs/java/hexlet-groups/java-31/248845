package exercise;

import exercise.connections.Connected;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

import java.util.List;
import java.util.ArrayList;

// BEGIN
public class TcpConnection {
    private final String ip;
    private final int port;
    private Connection state;

    TcpConnection(String ip, int port) {
        this.ip = ip;
        this.port = port;
        state = new Disconnected(this);
    }

    public String getCurrentState() {
        return state.getCurrentState();
    }

    public void connect() {
        state.connect();
    }

    public void disconnect() {
        state.disconnect();
    }

    public void write(String str) {
        state.write(str);
    }

    public void setState(Connection state) {
        this.state = state;
    }
}
// END
