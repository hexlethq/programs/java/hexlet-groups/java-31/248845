package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection {
    private final TcpConnection connection;

    public Disconnected(TcpConnection connection) {
        this.connection = connection;
    }

    @Override
    public String getCurrentState() {
        return "disconnected";
    }

    @Override
    public void connect() {
        connection.setState(new Connected(connection));
        System.out.println("Connection established.");
    }

    @Override
    public void disconnect() {
        System.out.println("Error: Try to disconnect when connection already disconnected.");
    }

    @Override
    public void write(String str) {
        System.out.println("Error: Try to write to disconnected connection.");
    }
}
// END
