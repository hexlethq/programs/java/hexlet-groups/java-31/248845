package exercise;

import java.awt.desktop.PrintFilesEvent;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> result = new ArrayList<>();
        for (Field field: address.getClass().getDeclaredFields()) {
            field.setAccessible(true);

            for (Annotation annotation: field.getDeclaredAnnotations()) {
                Class<? extends Annotation> type = annotation.annotationType();
                if (type.getName().contains("NotNull")) {
                    try {
                        if (field.get(address) == null) {
                            result.add(field.getName());
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return result;
    }

    public static Map<String, List<String>> advancedValidator(Address address) {
        Map<String, List<String>> notValidFields = new HashMap<>();

        for (Field field: address.getClass().getDeclaredFields()) {
            field.setAccessible(true);

            for (Annotation annotation: field.getDeclaredAnnotations()) {
                Class<? extends Annotation> type = annotation.annotationType();
                if (type.getName().contains("NotNull")) {
                    try {
                        if (field.get(address) == null) {
                            notValidFields.put(field.getName(), List.of("cannot be null"));
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                if (type.getName().contains("MinLength")) {
                    int minLengthValue = 0;
                    MinLength minLength = field.getAnnotation(MinLength.class);
                    if (minLength != null) {
                        minLengthValue = minLength.minLength();
                    }
                    try {
                        String fieldValue = (String) field.get(address);
                        if (fieldValue.length() < minLengthValue) {
                            notValidFields.put(field.getName(), List.of(String.format("length less than %d", minLengthValue)));
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return notValidFields;
    }
}
// END
