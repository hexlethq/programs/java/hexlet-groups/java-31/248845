package exercise;

import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.Map;

// BEGIN
class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        return emails.stream()
                .filter(x -> x.matches(".*@gmail\\.com")
                || x.matches(".*@yandex\\.ru")
                || x.matches(".*@hotmail\\.com"))
                .count();
    }
}
// END
