package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
class App {
    public static void swapKeyValue(KeyValueStorage storage) {
        Map<String, String> swappable = new HashMap<>(storage.toMap());
        for (String key: swappable.keySet()) {
            storage.unset(key);
            storage.set(swappable.get(key), key);
        }
    }
}
// END
